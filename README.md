# cas_explore

This report explores the current situation of Chat&Shop. Key findings include:
* Daily volume and conversion rate since the time of launch
* 40% of chats *bounced* (have 2 or less inbound messages)
* Chats that went overnight and/or have outbound response time above 30 minutes greatly affect conversion rate
* There are very few spam messages so it might not be worthwhile to automatically filter them yet
* Only 20% of customers return to Chat&Shop; we might want to bring them back more often